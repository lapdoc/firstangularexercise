import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ImageGeneratorComponent } from './image-generator/image-generator.component';
import { ImageGeneratorHistoryComponent } from './image-generator-history/image-generator-history.component';

@NgModule({
  declarations: [
    AppComponent,
    ImageGeneratorComponent,
    ImageGeneratorHistoryComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
