import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageGeneratorHistoryComponent } from './image-generator-history.component';

describe('ImageGeneratorHistoryComponent', () => {
  let component: ImageGeneratorHistoryComponent;
  let fixture: ComponentFixture<ImageGeneratorHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageGeneratorHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageGeneratorHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
