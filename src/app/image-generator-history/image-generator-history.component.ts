import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-generator-history',
  templateUrl: './image-generator-history.component.html',
  styleUrls: ['./image-generator-history.component.css']
})
export class ImageGeneratorHistoryComponent implements OnInit {

  @Input() PictureHistory;

  constructor() { }

  ngOnInit() {
  }

}
