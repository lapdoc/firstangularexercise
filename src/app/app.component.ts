import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-exercise';
  imageHistory = [];

  historyEventHandler(opts) {
    let x = {
      category: opts.selOpts.optionText,
      width: opts.picOpts.width,
      height: opts.picOpts.height
    }
    this.imageHistory.push(x);
  }
}
