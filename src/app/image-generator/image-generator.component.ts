import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-image-generator',
  templateUrl: './image-generator.component.html',
  styleUrls: ['./image-generator.component.css']
})
export class ImageGeneratorComponent implements OnInit {
  
  @Output() historyEvent = new EventEmitter();

  pictureOptions = {
    picUrl: "",
    width: 100,
    height: 100,
    selectedFormat: "JPG",
    bgColor: "008000",
    fgColor: "FF0000",
    pictureText: "This will show on the picture"
  };
  
  isSameWidthAndHeight = false;

  selectedOption = {
    valueUrl: '',
    optionText: ''
  };

  urlOptions = [
    { valueUrl: 'https://via.placeholder.com', optionText: 'placeholder' },
    { valueUrl: 'https://picsum.photos', optionText: 'general' },
    { valueUrl: 'https://placekitten.com', optionText: 'kitten' },
    { valueUrl: 'https://placebear.com', optionText: 'bear' },
    { valueUrl: 'https://placebeard.it', optionText: 'beard' },
    { valueUrl: 'http://www.fillmurray.com', optionText: 'Phil' },
    { valueUrl: 'https://www.placecage.com', optionText: 'Nicholas' },
    { valueUrl: 'https://stevensegallery.com', optionText: 'Steven' }
  ];

  pictureFormats = ['JPG', 'PNG', 'GIF'];

  constructor() { }

  ngOnInit() {
  }

  imageGeneratorHandler() {
    this.pictureOptions.picUrl = `${this.selectedOption.valueUrl}/${this.pictureOptions.width}/${this.pictureOptions.height}`;
    let opts = {
      picOpts: this.pictureOptions,
      selOpts: this.selectedOption
    }
    this.historyEvent.emit(opts);
  }

  placeHolderButtonHandler() {
    let concatString = this.pictureOptions.pictureText.split(' ').join('+');
    console.log(concatString);
    this.pictureOptions.picUrl = `${this.selectedOption.valueUrl}/${this.pictureOptions.width}x${this.pictureOptions.height}/${this.pictureOptions.bgColor.substr(1)}/${this.pictureOptions.fgColor.substr(1)}.${this.pictureOptions.selectedFormat}?text=${concatString}`;
    let opts = {
      picOpts: this.pictureOptions,
      selOpts: this.selectedOption
    }
    this.historyEvent.emit(opts);
  }

  tickBoxHandler() {
    if(this.isSameWidthAndHeight == true) {
      this.pictureOptions.height = this.pictureOptions.width;
    } 
  }

}
